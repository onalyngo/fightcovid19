import React, { useState } from "react";
import CovidStats from "./components/CovidStats";
import { Button } from "reactstrap";
import Symptoms from "./components/Symptoms";
import Measures from "./components/Measures";
import Contact from "./components/Contact";

const App = props => {
  const [showSymptoms, setShowSymptoms] = useState(false);
  const [showCountries, setShowCountries] = useState(false);
  const [showContacts, setShowContacts] = useState(false);
  const [showMeasures, setShowMeasures] = useState(false);

  const toggleSymptoms = () => {
    setShowSymptoms(false);
  };

  const toggleCountries = () => {
    setShowCountries(false);
  };

  const toggleContacts = () => {
    setShowContacts(false);
  };

  const toggleMeasures = () => {
    setShowMeasures(false);
  };

  return (
    <div className="vh-100 vw-100 d-flex justify-content-center align-items-center flex-column">
      <div
        className="border border-primary"
        style={{ width: "80%", height: "10%" }}
      >
        <h1 className="text-center my-1">FIGHT COVID-19</h1>
      </div>
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ width: "80%", height: "50%" }}
      >
        <div style={{ width: "70%", height: "100%" }}>
          <h2 className="text-center my-3">What is COVID-19?</h2>
          <p className="text-center my-3">
            A mild to severe respiratory illness that is caused by a coronavirus
            (Severe acute respiratory syndrome coronavirus 2 of the genus
            Betacoronavirus), is transmitted chiefly by contact with infectious
            material (such as respiratory droplets), and is characterized
            especially by fever, cough, and shortness of breath and may progress
            to pneumonia and respiratory failure
          </p>
        </div>
        <div
          className="d-flex justify-content-center align-items-center bg-primary"
          style={{ width: "30%", height: "100%" }}
        >
          <img
            src="https://png.pngtree.com/png-clipart/20200226/original/pngtree-coronavirus-danger-health-epidemic-medicine-human-pneumonia-vector-illustration-png-image_5316891.jpg"
            style={{ height: "100%", width: "100%" }}
            alt=""
          />
        </div>
      </div>
      <div>
        <div>
          <Button onClick={() => setShowCountries(!showCountries)}>
            Affected Countries
          </Button>
          <CovidStats
            showCountries={showCountries}
            toggleCountries={toggleCountries}
          />
          <Button onClick={() => setShowContacts(!showContacts)}>
            Who to Call?
          </Button>
          <Contact
            showContacts={showContacts}
            toggleContacts={toggleContacts}
          />
        </div>
        <div>
          <Button onClick={() => setShowSymptoms(!showSymptoms)}>
            Symptoms
          </Button>
          <Symptoms
            showSymptoms={showSymptoms}
            toggleSymptoms={toggleSymptoms}
          />
          <Button onClick={() => setShowMeasures(!showMeasures)}>
            Preventive Measures
          </Button>
          <Measures
            showMeasures={showMeasures}
            toggleMeasures={toggleMeasures}
          />
        </div>
      </div>
      <div>
        <CovidStats />
      </div>
    </div>
  );
};

export default App;