import React from "react";
import { Modal, ModalHeader, Table } from "reactstrap";

const Contact = props => {
  return (
    <Modal isOpen={props.showContacts}>
      <ModalHeader toggle={props.toggleContacts}>
        {/* <Table>
          <thead>
            <tr>Who to call?</tr>
          </thead>
          <tbody>
            <tr>
              <td>Fever</td>
              <td>Cough</td>
              <td>Shortness of breath</td>
            </tr>
            <tr>
              <td>Persistent pain or pressure in the chest</td>
              <td>New confusion or inability to arouse</td>
              <td></td>
            </tr>
          </tbody>
        </Table> */}
        <h3>Who to call?</h3>
        <div className='d-flex justify-content-center flex-column text-center'>
          <h2 className='text-danger'>COVID-19 hotlines</h2>
          <h4 className='text-info'>1555 (PLDT, Smart, Sun, and TnT)</h4>
          <h4 className='text-info'>(02) 894-26843 (894-COVID)</h4>
        </div>
      </ModalHeader>
    </Modal>
  );
};

export default Contact;