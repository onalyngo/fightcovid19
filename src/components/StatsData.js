const Countries = [
    {
      country: "USA",
      cases: 244433,
      recovered: 10403,
      died: 6063
    },
    {
      country: "Italy",
      cases: 115242,
      recovered: 18278,
      died: 13915
    },
    {
      country: "Spain",
      cases: 112065,
      recovered: 26743,
      died: 10348
    },
    {
      country: "China",
      cases: 81589,
      recovered: 76408,
      died: 3318
    },
    {
      country: "UK",
      cases: 33718,
      recovered: 135,
      died: 2921
    },
    {
      country: "Canada",
      cases: 11283,
      recovered: 1979,
      died: 173
    },
    {
      country: "South Korea",
      cases: 9976,
      recovered: 5828,
      died: 169
    },
    {
      country: "Japan",
      cases: 2495,
      recovered: 472,
      died: 62
    },
    {
      country: "France",
      cases: 59105,
      recovered: 12428,
      died: 5387
    },
    {
      country: "Philippines",
      cases: 2633,
      recovered: 51,
      died: 107
    }
  ];
  
  export default Countries;