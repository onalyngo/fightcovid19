import React from "react";
import Countries from "./StatsData";
// import CovidStats2 from "./CovidStats2";
import { Table, Modal, ModalHeader, Button } from "reactstrap";

const CovidStats = props => {
  return (
    <Modal isOpen={props.showCountries} >
      <ModalHeader toggle={props.toggleCountries}>
        <Table>
          <thead>
            <tr>
              <td>Countries</td>
              <td>Cases</td>
              <td>Recovered</td>
              <td>Died</td>
              {/* <td>
                <Button
                  style={{ width: "100px" }}
                  color="success"
                  className="my-1"
                >
                  Add a Country
                </Button>
              </td> */}
              <td></td>
            </tr>
          </thead>
          <tbody>
            {Countries.map((country, index) => (
              <tr key={index} className='text-danger'>
                <td>
                  <p>{country.country}</p>
                </td>
                <td>
                  <p>{country.cases}</p>
                </td>
                <td>
                  <p>{country.recovered}</p>
                </td>
                <td>
                  <p>{country.died}</p>
                </td>
                <td></td>
                {/* <td>
                  <Button
                    style={{ width: "75px" }}
                    color="danger"
                    className="my-1"
                  >
                    Delete
                  </Button>
                </td>
                <td>
                  <Button
                    style={{ width: "75px" }}
                    color="info"
                    className="my-1"
                  >
                    Edit
                  </Button>
                </td> */}
              </tr>
            ))}
          </tbody>
        </Table>
      </ModalHeader>
    </Modal>
  );
};

export default CovidStats;