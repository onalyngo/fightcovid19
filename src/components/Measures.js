import React from "react";
import { Modal, ModalHeader, Table } from "reactstrap";

const Measures = props => {
  return (
    <Modal isOpen={props.showMeasures} size="lg">
      <ModalHeader toggle={props.toggleMeasures}>
        <h3>Preventive Measures</h3>
        <ul className='text-info'>
          <li>The best way to prevent illness is to avoid being exposed to this virus.</li>
          <li>Avoid close contact with people who are sick.</li>
          <li>Cover you cough or sneeze with a tissue, then throw the tissue in the trash.</li>
          <li>Avoid touching your eyes, nose, and mouth.</li>
          <li>Clean and disinfect frequently touched objects and surfaces.</li>
          <li>Stay home when you are sick, except to get medical care.</li>
          <li>Wash your hands often with soap and water for at least 20 seconds.</li>
        </ul>

      </ModalHeader>
    </Modal>
  );
};

export default Measures;