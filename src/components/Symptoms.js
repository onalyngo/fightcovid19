import React from "react";
import { Modal, ModalHeader, Table } from "reactstrap";

const Symptoms = props => {
  return (
    <Modal isOpen={props.showSymptoms} size="lg">
      <ModalHeader toggle={props.toggleSymptoms}>
        {/* <Table>
          <thead>
            <tr>Symptoms</tr>
          </thead>
          <tbody>
            <tr>
              <td>Fever</td>
              <td>Cough</td>
              <td>Shortness of breath</td>
            </tr>
            <tr>
              <td>Persistent pain or pressure in the chest</td>
              <td>New confusion or inability to arouse</td>
              <td>Bluish lips or face</td>
            </tr>
          </tbody> */}
          <h3>Symptoms</h3>
        <ul className='text-info'>
          <li>Fever</li>
          <li>Cough</li>
          <li>Shortness of breath.</li>
          <li>Persistent pain or pressure in the chest.</li>
          <li>New confusion or inability to arouse.</li>
          <li>Bluish lips or face.</li>
        </ul>
      </ModalHeader>
    </Modal>
  );
};

export default Symptoms;